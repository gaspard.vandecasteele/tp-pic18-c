/*
 * File:   main.c
 * Author: Gaspard
 *
 * Created on 17 janvier 2022, 08:41
 */


#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "general.h"
#include "leds.h"

static __interrupt() void interrupt_handler(void);
static void chenillard();
static inline void config_bit(void);

static uint8_t led_activate = 0x02;
static uint8_t TMR0_flag = FALSE;

static __interrupt() void interrupt_handler(void)
{
    if(INTCONbits.INT0IF)
    {

            INTCONbits.INT0IF = OFF;
            TMR0 = 0x00;
            T0CONbits.TMR0ON = !T0CONbits.TMR0ON;   
    } 
    if(INTCONbits.TMR0IF) {
        INTCONbits.TMR0IF = OFF;
        //LED2_STATE = !LED2_STATE;
        led_activate <<= 1;
        TMR0_flag = TRUE;
    }
}

static void chenillard() {
    led_activate &= 0x0F;
    led_activate += -!led_activate & 0x02; //if led_activate == 0
                                           //then led_activate = 0x02
    LATB = led_activate;
}

static inline void config_bit(void)
{
    TRISBbits.TRISB0 = INP; //RB0 set as INPUT (button)
    TRISAbits.TRISA4 = OUTP; //RA4 set as OUTPUT (timer)
    INTCONbits.GIE = SET; //Enable all interuption
    INTCONbits.PEIE = SET; //enable all interutpion
    T0CONbits.TMR0ON = ON; //enable TMR0 interuption
    T0CONbits.T08BIT = FALSE; //8/16 bit flag
    T0CONbits.T0CS = FALSE; //T0CS: Timer0 Clock Source Select bit
                            //1 = Transition on T0CKI pin
                            //0 = Internal instruction cycle clock (CLKOUT)
    T0CONbits.PSA = FALSE; //PSA: Timer0 Prescaler Assignment bit
                           //1 = TImer0 prescaler is NOT assigned. Timer0 clock input bypasses prescaler.
                           //0 = Timer0 prescaler is assigned. Timer0 clock input comes from prescaler output.
    T0CONbits.T0PS = 0x01; //Prescaller
    INTCONbits.TMR0IE = ON; // Enable timer0 overflow interrupt
    INTCONbits.INT0IE = ON; //enable INT0 interuption
    INTCONbits.INT0IF = CLEAR; //clear INT0 interuption flag
    INTCONbits.TMR0IF = OFF; //clear TMR0 overflow bit
    INTCON2 = 0x00;
    
    
    LED1_DIR = OUTP; //SET LED1 as OUTPUT
    LED2_DIR = OUTP; //SET LED2 as OUTPUT
    LED3_DIR = OUTP; //SET LED3 as OUTPUT
    LED1_STATE = OFF; //TURN OFF LED1
    LED2_STATE = OFF; //TURN OFF LED2
    LED3_STATE = OFF; //TURN OFF LED3
    return;
}

void main(void) {
    Nop();
    config_bit();
    while(TRUE)
    {
        if(TMR0_flag)
        {
            chenillard();
            TMR0_flag = FALSE;
        }
    }
    return;
}
